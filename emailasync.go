package notify

import (
	"log"
	"net"
	"net/smtp"
	"sync"
	"time"
)

/*
This sender allows both synchronous, the default, and async' email sending.

Just use directly for synchronous sends, or call AsyncSender to get an
EmailSender that will only block when there are more than maxAsync clients
waiting for an SMTP client in the pool to become available.

Because only maxAsync async email requests can be queued, async email sends can
end up blocking, this is done to cap the memory consumption for queuing emails.

A nice enhancement would be to allow more than 1 SMTP client in the pool.
This was unnecessary for our traffic volumes.
*/
type EmailCore struct {
	maxAsyncSem chan int // semaphore to limit max async sends
	serverAddr  string
	auth        smtp.Auth
	sendWG      sync.WaitGroup
}

func NewEmailCore(maxAsync int, addr, user, pass string) *EmailCore {
	var auth smtp.Auth
	if user != "" {
		serverName, _, _ := net.SplitHostPort(addr)
		auth = smtp.PlainAuth("", user, pass, serverName)
	}

	return &EmailCore{
		maxAsyncSem: make(chan int, maxAsync),
		serverAddr:  addr,
		auth:        auth,
	}
}

/*
Make sure Sync/Async callers have finished and aren't going to call Send
any more.

This will wait for pending sync/async senders to complete their already queued
work and then close off the client connections
*/
func (c *EmailCore) Finish() {
	// sleep to allow any pending senders to get into the queues
	time.Sleep(time.Millisecond)

	log.Printf("INFO EmailCore FINISH: Async queue has %d pending\n", len(c.maxAsyncSem))

	// get our async queue slot
	select {
	case c.maxAsyncSem <- 1:
		// we've got our queue slot
	default:
		log.Println("INFO EmailCore FINISH: Blocking on async sender queue")
		// get our queue slot
		c.maxAsyncSem <- 1
	}

	// now wait for remaining senders
	log.Println("INFO EmailCore FINISH: Blocking on sends")
	c.sendWG.Wait()
}

/*
This is the Synchronous send that uses the client pool to allow faster sending.
*/
func (c *EmailCore) SendEmail(from string, to []string, msg []byte) error {
	c.sendWG.Add(1)
	defer c.sendWG.Done()
	return smtp.SendMail(c.serverAddr, c.auth, from, to, msg)
}

/*
Get a client that only blocks if there are too many emails already queuing.

Note: A INFO message is logged when a full-queue scenario is encountered.
*/
func (c *EmailCore) AsyncSender() EmailSender {
	return &asyncSender{c}
}

type asyncSender struct {
	core *EmailCore
}

func (c *asyncSender) SendEmail(from string, to []string, msg []byte) error {
	// try to add ourselves to the queue, will block if full
	// this limits the number of goroutines that can be created
	// as part of this
	c.core.maxAsyncSem <- 1

	// in goroutine
	go func() {
		// defer so that we're sure it happens
		defer func() {
			// release our queue spot
			<-c.core.maxAsyncSem
		}()

		c.core.SendEmail(from, to, msg)
	}()

	return nil
}
