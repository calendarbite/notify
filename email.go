package notify

/*
Basic interface to allow different implementations of email sending.
*/
type EmailSender interface {
	SendEmail(from string, to []string, msg []byte) error
}

type EmailSenderFunc func(from string, to []string, msg []byte) error

func (f EmailSenderFunc) SendEmail(from string, to []string, msg []byte) error {
	return f(from, to, msg)
}
