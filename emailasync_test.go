package notify

import (
	"bitbucket.org/calendarbite/notify/smtptest"
	"fmt"
	"reflect"
	"sort"
	"testing"
)

// Allows sorting smtptest.MsgInfos. Mixed sync/async calls will put things
// out of order. It assumes that each message has the same content except for
// up to 5 trailing digits (must be 0 padded) that provide the desired order.
// E.g. "boring message 001", "boring message 003", "boring message 002" will
// be reordered 001, 002, 003
type ByNumber []*smtptest.MsgInfo

func (a ByNumber) Len() int      { return len(a) }
func (a ByNumber) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByNumber) Less(i, j int) bool {
	ilen := len(a[i].Msg)
	jlen := len(a[j].Msg)
	return string(a[i].Msg[ilen-5:]) < string(a[j].Msg[jlen-5:])
}

func Test_emailSendSync(t *testing.T) {
	collector := &smtptest.CollectHandler{}
	srv := smtptest.Start("", collector)

	// our sender
	sender := NewEmailCore(10, srv.Addr, "", "")

	// send off 10 emails sync, make sure we recieve all 10
	var wantMsgs []*smtptest.MsgInfo

	for i := 0; i < 10; i++ {
		from := fmt.Sprintf("from%d@test.calendarbite.com", i)
		to := []string{fmt.Sprintf("dude%d@test.calendarbite.com", i)}
		body := []byte(fmt.Sprintf("some message that is totally boring %d", i))

		// send it
		if err := sender.SendEmail(from, to, body); err != nil {
			t.Fatal(err)
		}

		// save it for checks
		wantMsgs = append(wantMsgs, &smtptest.MsgInfo{from, to, body, true})
	}

	sender.Finish()
	srv.Stop()

	if len(collector.Msgs) != len(wantMsgs) {
		t.Fatalf("Got %d messages, wanted %d", len(collector.Msgs), len(wantMsgs))
	}
	for i := range collector.Msgs {
		got := collector.Msgs[i]
		want := wantMsgs[i]
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Mail %d: Got %+v, want %+v", i, *got, *want)
		}
	}
}

func Test_emailSendASync(t *testing.T) {
	collector := &smtptest.CollectHandler{}
	srv := smtptest.Start("", collector)

	// our sender
	sender := NewEmailCore(10, srv.Addr, "", "")
	asender := sender.AsyncSender()

	// send off 10 emails sync, make sure we recieve all 10
	var wantMsgs []*smtptest.MsgInfo

	for i := 0; i < 10; i++ {
		from := fmt.Sprintf("from%03d@test.calendarbite.com", i)
		to := []string{fmt.Sprintf("dude%03d@test.calendarbite.com", i)}
		body := []byte(fmt.Sprintf("some message that is totally boring %03d", i))

		// send it
		if err := asender.SendEmail(from, to, body); err != nil {
			t.Fatal(err)
		}

		// save it for checks
		wantMsgs = append(wantMsgs, &smtptest.MsgInfo{from, to, body, true})
	}

	sender.Finish()
	srv.Stop()

	// sort the messages
	sort.Sort(ByNumber(collector.Msgs))

	if len(collector.Msgs) != len(wantMsgs) {
		t.Fatalf("Got %d messages, wanted %d", len(collector.Msgs), len(wantMsgs))
	}
	for i := range collector.Msgs {
		got := collector.Msgs[i]
		want := wantMsgs[i]
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Mail %d: Got %+v, want %+v", i, *got, *want)
		}
	}
}

func Test_emailSendMixedSync(t *testing.T) {
	collector := &smtptest.CollectHandler{}
	srv := smtptest.Start("", collector)

	// our sender
	sender := NewEmailCore(10, srv.Addr, "", "")
	asender := sender.AsyncSender()

	// send off 10 emails sync, make sure we recieve all 10
	var wantMsgs []*smtptest.MsgInfo

	for i := 0; i < 100; i++ {
		from := fmt.Sprintf("from%03d@test.calendarbite.com", i)
		to := []string{fmt.Sprintf("dude%03d@test.calendarbite.com", i)}
		body := []byte(fmt.Sprintf("some message that is totally boring %03d", i))

		// send it
		s := asender
		if i%2 == 0 {
			s = sender
		}
		if err := s.SendEmail(from, to, body); err != nil {
			t.Fatal(err)
		}

		// save it for checks
		wantMsgs = append(wantMsgs, &smtptest.MsgInfo{from, to, body, true})
	}

	sender.Finish()
	srv.Stop()

	if len(collector.Msgs) != len(wantMsgs) {
		t.Fatalf("Got %d messages, wanted %d", len(collector.Msgs), len(wantMsgs))
	}

	// sort the messages
	sort.Sort(ByNumber(collector.Msgs))

	// now compare
	for i := range collector.Msgs {
		got := collector.Msgs[i]
		want := wantMsgs[i]
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Mail %d: Got %+v, want %+v", i, *got, *want)
		}
	}
}

func Test_emailSendMixedSyncSlow(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode. This takes 5 minutes.")
	}

	collector := &smtptest.CollectHandler{}
	srv := smtptest.Start("", collector)

	// add in a 300ms delay for each mail to force congestion
	overrides := []smtptest.CmdResponse{
		{Delay: 30, Prefix: "MAIL", Code: 250, Parts: []string{"OK"}, Continue: true},
	}
	srv.SetOverrides(overrides)

	// our sender
	sender := NewEmailCore(10, srv.Addr, "", "")
	asender := sender.AsyncSender()

	// send off 10 emails sync, make sure we recieve all 10
	var wantMsgs []*smtptest.MsgInfo

	for i := 0; i < 300; i++ {
		from := fmt.Sprintf("from%03d@test.calendarbite.com", i)
		to := []string{fmt.Sprintf("dude%03d@test.calendarbite.com", i)}
		body := []byte(fmt.Sprintf("some message that is totally boring %03d", i))

		// send it
		s := asender
		if i%2 == 0 {
			s = sender
		}

		go func() {
			if err := s.SendEmail(from, to, body); err != nil {
				t.Fatal(err)
			}
		}()

		// save it for checks
		wantMsgs = append(wantMsgs, &smtptest.MsgInfo{from, to, body, true})
	}

	sender.Finish()
	srv.Stop()

	if len(collector.Msgs) != len(wantMsgs) {
		t.Fatalf("Got %d messages, wanted %d", len(collector.Msgs), len(wantMsgs))
	}

	// sort the messages
	sort.Sort(ByNumber(collector.Msgs))

	// now compare
	for i := range collector.Msgs {
		got := collector.Msgs[i]
		want := wantMsgs[i]
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Mail %d: Got %+v, want %+v", i, *got, *want)
		}
	}
}
