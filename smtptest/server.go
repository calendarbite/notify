package smtptest

import (
	"fmt"
	"net"
	"net/mail"
	"net/textproto"
	"strings"
	"sync"
	"time"
)

/*
How should we respond to a particular command
*/
type CmdResponse struct {
	Prefix   string   // the prefix used to match the command to this response
	Delay    int      // number of milliseconds to wait before responding
	RawRsp   string   // this trumps all and will be sent to the client instead of the normal response
	Code     int      // the code to reply with
	Parts    []string // the cmd parts to respond with
	Continue bool     // should normal processing continue after?
}

type CmdResponseSlice []CmdResponse

/*
Check for specific handling of this command
*/
func (rs CmdResponseSlice) Find(cmd string) *CmdResponse {
	for i, r := range rs {
		if strings.HasPrefix(cmd, r.Prefix) {
			return &rs[i]
		}
	}

	return nil
}

/*
Sends the response as requested, waiting if need be

Returns true if command processing should be continued as normal.
*/
func (r *CmdResponse) Send(c *textproto.Conn) bool {
	if r.Delay != 0 {
		time.Sleep(time.Millisecond * time.Duration(r.Delay))
	}

	if r.RawRsp != "" {
		c.W.Write([]byte(r.RawRsp))
		if err := c.W.Flush(); err != nil {
			panic(err)
		}
	} else {
		sendResp(c, r.Code, r.Parts)
	}

	return r.Continue
}

/*
Captures info about a single email received by the server.

Messages can be implicitly closed, by the connection closing or a reset command
being received. Closed is true only if the message body was receieved in full
and properly terminated. Close
*/
type MsgInfo struct {
	From     string
	Rcpts    []string // list of recipients
	Msg      []byte   // the message body
	Complete bool     // true if we got everything (at least 1 RCPT and a valid DATA)
}

type Handler interface {
	OnEmail(*MsgInfo)
}

type HandlerFunc func(*MsgInfo)

func (f HandlerFunc) OnEmail(mi *MsgInfo) {
	f(mi)
}

func newListener(addr string) net.Listener {
	if addr != "" {
		l, err := net.Listen("tcp", addr)
		if err != nil {
			panic(fmt.Sprintf("smtptest: failed to listen on %v: %v", addr, err))
		}
		return l
	}
	l, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		if l, err = net.Listen("tcp6", "[::1]:0"); err != nil {
			panic(fmt.Sprintf("smtptest: failed to listen on a port: %v", err))
		}
	}
	return l
}

/*
Implements a super basic SMTP server that will allow connections in
and collect emails is receives into a list for verification.

Correct message ordering and connection close can also be verified.
*/
type Server struct {
	Addr     string // the address this server is listening on
	Listener net.Listener
	Handler  Handler

	rspOverrides CmdResponseSlice
	sync.Mutex   // protects nextProps

	// Counts the number of outstanding Message requests on this server.
	// Close blocks until all requests are finished.
	wg sync.WaitGroup
}

/*
Creates a new SMTP server, listening on addr. Addr must include the port and can
optionally include the IP to bind on. If addr == "", a system assigned port on
local loopback interface is used
*/
func Start(addr string, handler Handler) *Server {

	if handler == nil {
		panic("A handler is required")
	}

	ts := &Server{
		Listener: newListener(addr),
		Handler:  handler,
	}
	ts.Start()
	return ts
}

func (s *Server) Start() {
	if s.Addr != "" {
		panic("Server already started")
	}
	s.Addr = s.Listener.Addr().String()

	fmt.Printf("INFO Starting SMTP test server on %s\n", s.Addr)
	go s.Serve()
}

func (s *Server) Serve() {
	defer s.Listener.Close()

	for {
		c, err := s.Listener.Accept()
		if err != nil {
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				time.Sleep(5 * time.Millisecond)
				continue
			}
			if oe, ok := err.(*net.OpError); ok && strings.Contains(oe.Error(), "closed") {
				// a normal socket closed, so Accept bailed
				break
			}

			// some other error
			panic(err)
		}
		go s.handleConnection(c, s.Handler)
	}
}

func (s *Server) Stop() {
	fmt.Printf("INFO BEGIN SMTP test server Stop on Addr %s\n", s.Addr)

	// close off to stop new requests
	s.Listener.Close()

	// wait till pending recieves are done
	s.wg.Wait()

	fmt.Printf("INFO END SMTP test server Stop on Addr %s\n", s.Addr)
}

/*
Set how the server should respond to MAIL commands (and it's RCPT/DATA).

Note: Only allows overriding the response and timing of the MAIL command right
now.
*/
func (s *Server) SetOverrides(cr []CmdResponse) {
	s.Lock()
	s.rspOverrides = CmdResponseSlice(cr)
	s.Unlock()
}

/*
Waits for the next command to be received. Returns an error if the command is
not what was expected, i.e. doesn't start with req.
*/
func waitForCmd(c *textproto.Conn) (string, error) {
	id := c.Next()
	c.StartRequest(id)
	defer c.EndRequest(id)
	return c.ReadLine()
}

func sendOk(c *textproto.Conn) error {
	return sendResp(c, 250, []string{"OK"})
}

func sendResp(c *textproto.Conn, code int, lines []string) error {
	if len(lines) < 1 {
		panic("Must send at least 1 line back")
	}

	lastLine := lines[len(lines)-1]
	lines = lines[:len(lines)-1]

	// send with "code-line"
	for _, l := range lines {
		fmt.Fprintf(c.W, "%d-%s\r\n", code, l)
	}

	// send last line, "code line\r\n\r\n"
	fmt.Fprintf(c.W, "%d %s\r\n", code, lastLine)
	return c.W.Flush()
}

/*
Dumb method that just waits for EHLO, then handles MAIL requests until
a QUIT request is received.
*/
func (s *Server) handleConnection(conn net.Conn, handler Handler) {
	// get a text proto
	c := textproto.NewConn(conn)
	defer c.Close()

	// send the greeting
	if err := sendResp(c, 220, []string{"localhost golangsmtptestserver"}); err != nil {
		panic(err)
	}

	// wait for ELHO - ReadLine
	if cmd, err := waitForCmd(c); err != nil {
		panic(err)
	} else if !strings.HasPrefix(cmd, "EHLO") {
		panic(fmt.Errorf("Got %s, want EHLO", cmd))
	}
	if err := sendResp(c, 250, []string{"email-localhost", "8BITMIME", "SIZE 10485760", "OK"}); err != nil {
		panic(err)
	}

	// wait for the emails
	for {
		var from string

		// wait for MAIL FROM commands
		if cmd, err := waitForCmd(c); err != nil {
			panic(err)
		} else if strings.HasPrefix(cmd, "RSET") {
			continue
		} else if strings.HasPrefix(cmd, "QUIT") {
			if err := sendResp(c, 221, []string{"OK"}); err != nil {
				panic(err)
			}
			break
		} else if !strings.HasPrefix(cmd, "MAIL FROM:") {
			panic(fmt.Errorf("Got %s, want MAIL FROM:", cmd))
		} else {
			// parse out the FROM
			addr, err := mail.ParseAddress(cmd[10:])
			if err != nil {
				from = cmd[10:]
			} else {
				from = addr.Address
			}
		}

		// grab how we're meant to behave
		s.Lock()
		rspOverrides := s.rspOverrides
		s.Unlock()

		// response with override or normal
		if resp := rspOverrides.Find("MAIL"); resp != nil {
			if !resp.Send(c) {
				return
			}
		} else if err := sendOk(c); err != nil {
			panic(err)
		}

		// now actually handle the MAIL command
		mi, err := s.handleMailCmd(c, from, rspOverrides)
		if mi != nil {
			handler.OnEmail(mi)
		}
		if err != nil {
			panic(err)
		}
	}
}

func (s *Server) handleMailCmd(c *textproto.Conn, from string, rspOverrides CmdResponseSlice) (*MsgInfo, error) {
	s.wg.Add(1)
	defer s.wg.Done() // a defer, in case we panic

	mi := &MsgInfo{From: from}

	// now we want 1 or more RCP - ReadLine and fork on "RCPT TO" vs "DATA"
	// RSET can also come in at almost any time and throw out the MAIL request
	for {
		if cmd, err := waitForCmd(c); err != nil {
			return mi, err
		} else if strings.HasPrefix(cmd, "RSET") {
			return nil, nil
		} else if strings.HasPrefix(cmd, "RCPT TO:") {
			addr, err := mail.ParseAddress(cmd[8:])
			if err != nil {
				mi.Rcpts = append(mi.Rcpts, cmd[8:])
			} else {
				mi.Rcpts = append(mi.Rcpts, addr.Address)
			}

			if err := sendOk(c); err != nil {
				return mi, err
			}
		} else if strings.HasPrefix(cmd, "DATA") {
			if len(mi.Rcpts) < 1 {
				return mi, fmt.Errorf("Got DATA command with 0 recipients")
			}

			// reply with "Please send us the body"
			if err := sendResp(c, 354, []string{"Start mail input"}); err != nil {
				return mi, err
			}

			// read in our data
			if msg, err := c.ReadDotBytes(); err != nil {
			} else {
				mi.Msg = msg[:len(msg)-1] // trim off the trailing \n ReadDotBytes adds
				mi.Complete = true
			}

			// tell 'em we're all good
			if err := sendOk(c); err != nil {
				return mi, err
			}

			break
		} else {
			return mi, fmt.Errorf("Got %s, want RCPT TO or DATA", cmd)
		}
	}

	return mi, nil
}
