package smtptest

import (
	"net/smtp"
	"reflect"
	"testing"
)

func Test_EHLO(t *testing.T) {
	t.Parallel()

	srv := Start("", &NullHandler{})

	// make a client
	c, err := smtp.Dial(srv.Addr)
	if err != nil {
		t.Fatal(err)
	}
	if err := c.Hello("localhost"); err != nil {
		t.Fatal(err)
	}
	if err := c.Quit(); err != nil {
		t.Fatal(err)
	}
}

func Test_1MAIL1RCPT(t *testing.T) {
	t.Parallel()

	from := "fakedude@gmail.com"
	to := []string{"test@test.calendarbite.com"}
	msg := []byte("Some boring message body that's actually invalid")

	collect := &CollectHandler{}
	srv := Start("", collect)

	if err := smtp.SendMail(srv.Addr, nil, from, to, msg); err != nil {
		t.Fatal(err)
	}

	// check we got our mail intact
	if len(collect.Msgs) != 1 {
		t.Fatalf("Got %d messages, want 1", len(collect.Msgs))
	}

	gotMsg := collect.Msgs[0]
	if gotMsg.Complete == false {
		t.Errorf("Message was not completed.")
	}

	if gotMsg.From != from {
		t.Errorf("Got From %q, want %q", gotMsg.From, from)
	}

	if len(gotMsg.Rcpts) != len(to) {
		t.Errorf("Got Rctps %v, want %v", gotMsg.Rcpts, to)
	}
	for i, r := range gotMsg.Rcpts {
		wantR := to[i]

		if r != wantR {
			t.Errorf("Rcpt %d, Got %s, want %s", i, r, wantR)
		}
	}

	if !reflect.DeepEqual(gotMsg.Msg, msg) {
		t.Errorf(`Got msg: "%s", wanted "%s"`, gotMsg.Msg, msg)
		t.Fatalf(`Got msg: "%v", wanted "%v"`, gotMsg.Msg, msg)
	}
}

func Test_MalformedResponse(t *testing.T) {
	t.Parallel()

	from := "fakedude@gmail.com"
	to := []string{"test@test.calendarbite.com"}
	msg := []byte("Some boring message body that's actually invalid")

	collect := &CollectHandler{}
	srv := Start("", collect)
	overrides := []CmdResponse{
		{Prefix: "MAIL", RawRsp: "wdinwdnidwin\r\n", Continue: false},
	}
	srv.SetOverrides(overrides)

	if err := smtp.SendMail(srv.Addr, nil, from, to, msg); err == nil {
		t.Fatalf("Wanted error, got Nil")
	} else {
		t.Log(err)
	}
}
