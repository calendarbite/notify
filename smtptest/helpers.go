package smtptest

import (
	"sync"
)

/*
Handler does nothing.
*/
type NullHandler struct {
}

func (h *NullHandler) OnEmail(msg *MsgInfo) {
}

type CollectHandler struct {
	Msgs       []*MsgInfo
	sync.Mutex // protects Msgs
}

func (h *CollectHandler) OnEmail(msg *MsgInfo) {
	h.Lock()
	h.Msgs = append(h.Msgs, msg)
	h.Unlock()
}
