package main

import (
	"bitbucket.org/calendarbite/notify/smtptest"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"
)

var flagAddr = flag.String("addr", "", "The ip:port that the server should bind. Blank == localhost:system assigned.")
var flagOutDir = flag.String("outdir", "", "A directory to log all received emails to.")

func main() {
	flag.Usage = usage
	flag.Parse()

	args := flag.Args()
	if len(args) == 1 && args[0] == "-h" {
		flag.Usage()
		return
	}

	// Ensure that outdir is a directory (or create it)
	outDir := *flagOutDir
	if outDir != "" {
		if err := os.MkdirAll(outDir, os.ModePerm); err != nil {
			panic(err)
		}
	}

	// simple printing handler
	emailnum := 0
	lastTime := ""
	h := smtptest.HandlerFunc(func(mi *smtptest.MsgInfo) {
		log.Printf("Message\n\tComplete: %v\n", mi.Complete)
		log.Printf("\tFrom: %s\n", mi.From)
		for _, to := range mi.Rcpts {
			log.Printf("\tTo: %s\n", to)
		}
		log.Printf("\tBody: %s\n", mi.Msg[:50])

		// if we're meant to log to files
		if outDir != "" {
			time := time.Now().Format(time.RFC3339)
			if time == lastTime {
				emailnum++
			} else {
				lastTime = time
				emailnum = 0
			}
			fname := fmt.Sprintf("%s_%04d.email", time, emailnum)
			// create the file
			f, err := os.Create(outDir + "/" + fname)
			if err != nil {
				log.Printf("ERROR: Could not write out email. " + err.Error())
				return
			}
			// dump out the info
			fmt.Fprintf(f, "Message\n\tComplete: %v\n", mi.Complete)
			fmt.Fprintf(f, "\tFrom: %s\n", mi.From)
			for _, to := range mi.Rcpts {
				fmt.Fprintf(f, "\tTo: %s\n", to)
			}
			fmt.Fprintf(f, "\tBody: %s\n", mi.Msg)
		}
	})

	// start the server goroutine
	srv := smtptest.Start(*flagAddr, h)

	// wait for a kill or term signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	log.Println(<-c)

	// close off the server
	srv.Stop()
}

func usage() {
	fmt.Println(`testsmtpserver is a SMTP that prints out the emails it receives.

Usage:
    testsmtpserver [options]

Options:
`)
	flag.PrintDefaults()
}
